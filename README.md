# Orge

## Étapes de développement du projet

1. **Réalisation d'un diagramme de Use Case** : Pour mieux comprendre les interactions entre les acteurs de l'application et les fonctionnalités qu'elle offre, nous avons créé un diagramme de Use Case. Ce diagramme nous a aidés à identifier les acteurs de l'application et à lister les principales fonctionnalités qu'elle propose.

2. **Écriture de 3-4 Use Cases détaillés** : Une fois que nous avions identifié les acteurs et les fonctionnalités de l'application, nous avons écrit 3 à 4 Use Cases détaillés pour décrire en profondeur les interactions entre les acteurs et le système. Chaque Use Case a inclus une description, les acteurs impliqués, les étapes du scénario, les préconditions et les post-conditions.

3. **Réalisation d'un diagramme d'activité** : Pour détailler davantage les scénarios de 2 à 3 fonctionnalités spécifiques, nous avons créé des diagrammes d'activité. Ces diagrammes nous ont permis de visualiser les étapes et les décisions prises lors de l'exécution d'une tâche ou d'une fonctionnalité.

4. **Identification des entités et réalisation d'un diagramme de classe** : Pour représenter les données de notre application et leurs relations, nous avons identifié les entités principales (objets, tables, etc.) et avons créé un diagramme de classe. Ce diagramme a montré les entités, leurs attributs et leurs relations, ce qui a facilité la compréhension de la structure de données de notre application.

5. **Création d'un diagramme de classes des différentes couches du backend dans le cas d'une API Rest** : Comme notre projet comprend une API REST, nous avons créé un diagramme de classes pour représenter les différentes couches du backend, y compris les contrôleurs, les services, les modèles de données, etc. Cela a permis de visualiser l'architecture de notre application.

# Organisation 

Nous avons réparti les tâches entre Hisami, Loïs, Sébastien et Alexandre avec les issues de GITLabs.
Chaque début d'après-midi on se concertait afin de définir qui faisait certaines tâches, pour les fonctions complexes, le travail de groupe à été privilégier.

# Use Case:

```markdown
- Use Case Global:
```
![<>](</img/Global_Use_Case.png>)

```markdown
- Use Case Visiteur:
```
![<>](</img/Visiteur_Use_Case.png>)

```markdown
- Use Case Developpeur Connecté:
```
![<>](</img/DevConnecté_Use_Case.png>)

```markdown
- Use Case Porteur Projet Connecté:
```
![<>](</img/PorteurProjetConnecté_Use_Case.png>)

```markdown
- Use Case Paiement Sécurisé:
```
![<>](</img/paiementsecu.png>)


# 3 (trois) Use Case Détaillés (cas d'utilisation):

| Cas d'utilisation       | Consulter les développeurs               |
|--------------------------|------------------------------------------|
| Référence (ID)          | 01                                       |
| Acteur(s) Principal(aux)| - Visiteur                               |
|                         | - Porteur de projets connectés           |
| Acteur(s) Secondaire(s) | - Aucun                                  |
| Date de création        | 02-10-2023                               |
| Responsable             | Jean Demel                               |
| Pré-condition           | - Site internet fonctionnel              |
|                         | - Contenu existant d'une liste de développeurs |
| Scénario Nominal        | 1. Le visiteur saisit une compétence technique ou métier. |
|                         | 2. Le visiteur sélectionne une localisation (optionnel). |
|                         | 3. Le visiteur valide la sélection.       |
| Post-Condition          | Une requête est envoyée au serveur pour demander l'affichage. |
| Scénarios alternatifs   | **A1**                                   |
|                         | 1. Le visiteur décide de filtrer sa requête. |
|                         | 2. Le visiteur filtre sa recherche par spécialité ou prix. |
|                         | 3. Le site affiche les résultats.         |
|                         | 4. Le visiteur peut sélectionner une fiche pour accéder aux détails. |
|                         | **A2**                                   |
|                         | 1. Le visiteur sélectionne plusieurs filtres. |
|                         | 2. Aucun résultat n'est affiché.           |
|                         | 3. Le site propose des profils proches de la requête demandée. |
|                         | **B1**                                   |
|                         | 1. Le visiteur clique sur son choix à l'étape 4 ou 7. |
|                         | 2. Le visiteur accède à la fiche détaillée du développeur à l'étape 5 ou 8. |

| Cas d'utilisation       | Proposer un projet                     |
|--------------------------|----------------------------------------|
| Référence (ID)          | 02                                     |
| Acteur(s) Principal(aux)| - Porteur de projets connectés        |
|                         | - Administrateurs                      |
| Acteur(s) Secondaire(s) | - Aucun                                |
| Date de création        | 02-10-2023                             |
| Responsable             | Jean Demel                             |
| Pré-condition           | - Site internet fonctionnel            |
|                         | - Utilisateur connecté                |
| Scénario Nominal        | 1. Donner un titre au projet           |
|                         | 2. Saisir les informations du projet (résumé, budget global, ...) |
|                         | 3. Préciser les technologies attendues |
|                         | 4. Préciser l'expérience attendue      |
|                         | 5. Préciser le nombre de développeurs nécessaires |
|                         | 6. Définir un délai de réalisation     |
|                         | 7. L'administrateur valide le projet   |
|                         | 8. Le porteur de projet propose le projet à des candidats |
| Post-Condition          | - Validation du traitement renvoyée par l'application au porteur de projet |
|                         | - Projet visible sur la liste des projets du site |
| Scénarios alternatifs   | **A1**                                 |
|                         | 1. Le projet est refusé par l'application (commence après le 7) |
|                         | 2. L'administrateur contacte le porteur pour redéfinir le projet |
|                         | 3. L'administrateur sanctionne si le projet n'est pas conforme aux règles du site |
|                         | 4. Si mise en conformité du projet, alors le porteur de projet propose le projet à des candidats |
|                         | **A2**                                 |
|                         | 1. En attente de validation car besoin de pièces complémentaires ou vérifications (commence après le 7) |
|                         | 2. L'administrateur envoie un message pour demander des pièces complémentaires |
|                         | 3. Le projet est mis en attente de complétion |
|                         | 4. Le porteur de projets doit retourner les pièces avant un délai défini |
|                         | 5. Si les pièces sont fournies, le projet est validé par l'administrateur |
|                         | 6. Le porteur de projet propose le projet à des candidats |

| Cas d'utilisation               | Recevoir proposition de projet et la traiter |
|--------------------------------|---------------------------------------------|
| Référence (ID)                 | 03                                          |
| Acteur(s) Principal(aux)       | - Développeur                              |
|                                | - Porteur de projet                        |
| Acteur(s) Secondaire(s)        | - Aucun                                    |
| Date de création               | 02-10-2023                                 |
| Responsable                    | Jean Demel                                 |
| Pré-condition                  | - Site internet fonctionnel                |
|                                | - Utilisateur connecté                    |
|                                | - Projet validé                            |
| Scénario Nominal               | 1. Être contacté par le porteur de projet. |
|                                | 2. Étude de l'offre par le développeur.   |
|                                | 3. Offre acceptée par le développeur.      |
|                                |    4. Création d'un devis au travers des outils de l'application. |
|                                |    5. Envoi au porteur de projet.          |
|                                |    6. En attente du retour de validation par le porteur de projet. |
|                                |    7. Acceptation du devis par le porteur de projet. |
|                                |    8. Information présente sur l'espace personnel du développeur. |
| Post-Condition                 | - Validation du traitement renvoyée par l'application au porteur de projet et au développeur. |
| Scénarios alternatifs          | **A1**                                      |
|                                | 1. Offre refusée par le développeur.       |
|                                | 2. Message complémentaire transmis au porteur de projet. |
|                                | 3. Informations qui peuvent être retrouvées sur l'historique du site. |
|                                | **A2**                                      |
|                                | 1. Refus du devis par le porteur de projet. |
|                                | 2. Négociation par message entre le porteur de projet et le développeur. |
|                                | 3. Proposition d'un nouveau devis.          |
|                                | 4. Acceptation du devis par le porteur de projet. |
|                                | 5. Information présente sur l'espace personnel du développeur. |
|                                | **A3**                                      |
|                                | 1. Refus du devis par le porteur de projet. |
|                                | 2. Négociation par message entre le porteur de projet et le développeur. |
|                                | 3. Fin de la relation professionnelle.      |

# Diagrammes d'Activités

```markdown
1. Inscription Visiteur:
```
![<>](</img/Inscription_Visiteur.png>)

```markdown
2. Payer frais gestion:
```
![<>](</img/Diagram_PayerFraisGestion.png>)

```markdown
3. Création devis Dev:
```
![<>](</img/Diagram_CreationDevisDev.png>)

# Diagrammes d'Entités

```markdown
- Diagramme d'Entités
```
![<>](</img/Diagram_Entitées.png>)


```markdown
- Diagramme Couche Entités (DAO)
```
![<>](</img/Diagram_Couches_Entitées.png>)


```markdown
- Diagramme Couche Controller
```
![<>](</img/Diagram_Couches_Controlleurs.png>)

# Le boss

![<Oui c'est moi le plus bô>](</img/gold.png>)

